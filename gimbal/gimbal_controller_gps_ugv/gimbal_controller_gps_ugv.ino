/******************************
   PAN-TILT SERVO CONTROL
   ----------------------
   Author(s):   Rolf Andrada
                Nathan Levigne
   Created:     11.2.17
   Edited:      11.4.17
 ******************************/

#include <Servo.h>
#include <math.h>
#include <ros.h>
#include <std_msgs/Float64MultiArray.h>

// Intialize ROS Node and variables
ros::NodeHandle nh;

Servo panServo;
Servo tiltServo;
int anVal;          //analog values
char panPin = 9;    //pan servo pin
char tiltPin = 10;  //tilt servo pin

// Initial servo position
double homeHeading = 90;         //servo mid-point
double panHeading = 90;          //current pan angle storage
double tiltHeading = 90;         //current tilt angle storage

// Callback function
void messageCb(const std_msgs::Float64MultiArray& msg) {
  panHeading = msg.data[0];
  tiltHeading = msg.data[1];
  point();
  digitalWrite(13, HIGH - digitalRead(13));
}

// Create subscribtion
ros::Subscriber<std_msgs::Float64MultiArray> sub("/uav/gps", &messageCb ); //Should be changed in the future

/*********************************************
   Function:    setup()
   Description: Initialize parameters.
   Notes:
 *********************************************/
void setup() {
  // Removed since the standard baud rate is 57600 for rosserial
  //Serial.begin(9600);

  pinMode(13, OUTPUT);
  nh.initNode();
  nh.subscribe(sub);

  // Set servo pins
  panServo.attach(panPin);
  tiltServo.attach(tiltPin);

  // Initialize servo position
  panServo.write(homeHeading);
  tiltServo.write(homeHeading);
}

/*********************************************
   Function:    loop()
   Description: Perform continuious control.
   Notes:       To stop jitter, try:
                pinMode(PIN, OUTPUT);
                myservo.write(degree);
                delay(5000);
                pinMode(PIN, INPUT);
 *********************************************/
void loop() {
  nh.spinOnce();
  delay(1);
}

/*********************************************
   Function:    point()
   Description: Point camera.
   Notes:
 *********************************************/
void point() {
  double panOffset, panSign, tiltOffset, tiltSign;
  
//  if ( -30 <= panHeading && panHeading < 0 )
//  {
//    panOffset  = 0;
//    panSign    = -1;
//    panHeading = -30;
//    tiltOffset = 0;
//    tiltSign = -1; 
//  }
//  if ( 0 <= panHeading && panHeading <= 30 )
//  {
//    panOffset  = 180;
//    panSign    = 1;
//    panHeading = 30;  
//    tiltOffset = 180;
//    tiltSign = 1;
//  }
//  if ( 150 <= panHeading && panHeading <= 180 )
//  {
//    panOffset  = 180;
//    panSign    = 1;
//    panHeading = 150;
//    tiltOffset = 180;  
//    tiltSign = 1;
//  }
//  if ( -180 < panHeading && panHeading <= -150 )
//  {
//    panOffset  = 0;
//    panSign    = 1;
//    panHeading = -150;
//    tiltOffset = 0;
//    tiltSign = -1; 
//  }
  if ( 0 < panHeading && panHeading < 180 )
  {
    panOffset  = 180;
    panSign    = 1;
    tiltOffset = 180;
    tiltSign = 1; 
  }
  if ( -180 < panHeading && panHeading < 0 )
  {
    panOffset  = 0;
    panSign    = -1; // should be negative... (0:180,-180:0)
    tiltOffset = 0;
    tiltSign = -1;   
  }
  
  panServo.write(panOffset - panHeading * panSign);
  tiltServo.write(tiltOffset - tiltHeading * tiltSign);
}




