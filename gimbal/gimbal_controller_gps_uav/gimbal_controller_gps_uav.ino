/******************************
   TILT SERVO CONTROL
   ----------------------
   Author(s):   Rolf Andrada
                Nathan Levigne
   Created:     2.18.18
   Edited:      2.19.18
 ******************************/

#include <Servo.h>
#include <math.h>
#include <ros.h>
#include <std_msgs/Float64.h>

// Intialize ROS Node and variables
ros::NodeHandle nh;

Servo tiltServo;
char tiltPin = 10;  //tilt servo pin
char fbPin = 0; //servo feedback pin

// Initial servo position
double homeHeading = 90;         //servo mid-point
double tiltHeading = 90;         //current tilt angle storage

double fbVal;

// Create publisher (Not yet tested)
std_msgs::Float64 fb_msg;
ros::Publisher pub("/servo_feed_back", &fb_msg);

// Callback function
void messageCb(const std_msgs::Float64& msg) {
  tiltHeading = msg.data; //Needs to be changed
  point();
  fbVal = analogRead(fbPin);
  fb_msg.data = fbVal/1024*PI;
  pub.publish( &fb_msg );
  digitalWrite(13, HIGH - digitalRead(13));
}

// Create subscribtion
ros::Subscriber<std_msgs::Float64> sub("/tilt_controller/command", &messageCb );

/*********************************************
   Function:    setup()
   Description: Initialize parameters.
   Notes:
 *********************************************/
void setup() {
  // Removed since the standard baud rate is 57600 for rosserial
  //Serial.begin(9600);

  pinMode(13, OUTPUT);
  nh.initNode();
  nh.subscribe(sub);
  nh.advertise(pub);

  // Set servo pins
  tiltServo.attach(tiltPin);

  // Initialize servo position
  tiltServo.write(homeHeading);
}

/*********************************************
   Function:    loop()
   Description: Perform continuious control.
   Notes:       To stop jitter, try:
                pinMode(PIN, OUTPUT);
                myservo.write(degree);
                delay(5000);
                pinMode(PIN, INPUT);
 *********************************************/
void loop() {
  nh.spinOnce();
  delay(1);
}

/*********************************************
   Function:    point()
   Description: Point camera.
   Notes:
 *********************************************/
void point() {
  tiltServo.write(90-tiltHeading);
}
